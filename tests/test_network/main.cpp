#define BOOST_TEST_MODULE network
#include <boost/test/included/unit_test.hpp>
#include "network.hpp"
#include <sstream>
#include <random>
#include <thread>
#include <condition_variable>
#include <mutex>

BOOST_AUTO_TEST_CASE( network_message)
{
    using namespace raspberry_guard::network;
    std::default_random_engine engine;
    std::uniform_int_distribution<> dist(0x00, 0xFF);
    auto generate_random_char { [&]() mutable { return dist(engine); } };

    std::vector<char> message_buf(1024);
    std::generate_n(std::begin(message_buf), std::size(message_buf), generate_random_char);

    network_message_raw message_output(std::move(message_buf));

    auto buf_output { network_message_adapter::to_vec(&message_output) };

    auto message_input { network_message_adapter::from_vec(buf_output) };

    bool result;

    if (auto mptr { dynamic_cast<network_message_raw*>(message_input.get()) }; mptr)
    {
        result = (message_output == (*mptr));
    }
    else result = false;
    BOOST_TEST( result );
}

BOOST_AUTO_TEST_CASE( tcp_client_server )
{
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(2s);
    using namespace raspberry_guard::network;
    async_tcp_server tcp_server(7990);
    tcp_server.run(2);
    bool status = true;
    BOOST_CHECK(tcp_server.is_running());
    status &= tcp_server.is_running();
    const std::vector<char> sent_message { 'h', 'o', 'p', 'e', ' ', 'i', 't', ' ', 'w', 'o', 'r', 'k','s', 0x1, 0x2, 0x3 };
    const std::vector<char> recv_message { 'v', 'l', 'a', 'd', 'i', 'm', 'i', 'r', ' ', 'p', 'u', 't', 'i', 'n' };
    bool end_of_transfer = false;
    auto write_callback { [&] (auto facade, auto endpoint, const auto& err) //! SERVER
        {
            BOOST_CHECK(!err);
            status &= !err;
        }};
    auto read_callback { [&](auto facade, auto endpoint, const auto& err, auto msg) //!SERVER
        {
            if (!end_of_transfer)
            {
                BOOST_CHECK(!err);
                status &= !static_cast<bool>(err);
                BOOST_CHECK(msg == sent_message);
                status &= msg == sent_message;
                facade->async_send_to(endpoint, recv_message);
            }
            else
            {
                BOOST_CHECK(err);
                status &= static_cast<bool>(err);
                BOOST_CHECK(msg.empty());
                status &= msg.empty();
            }
        } };
    tcp_server.register_read_callback(std::move(read_callback));
    tcp_server.register_write_callback(std::move(write_callback));
    boost::asio::io_service executor;
    async_tcp_client client(executor);
    auto send_callback { [&sent_message, &status](async_tcp_client* facade, const boost::system::error_code& err, std::size_t nbytes)
        {
            BOOST_CHECK(!err);
            status &= !static_cast<bool>(err);
            BOOST_CHECK(nbytes == sent_message.size());
            status &= nbytes == sent_message.size();
        }};
    auto recv_callback { [&recv_message, &end_of_transfer, &status] (async_tcp_client* facade, const boost::system::error_code& err, std::vector<char> msg)
        {
            if (!end_of_transfer)
            {
                BOOST_CHECK(!err);
                status &= !static_cast<bool>(err);
                BOOST_CHECK(msg == recv_message);
                status &= msg == recv_message;
                end_of_transfer = true;
                facade->close();
            }
            else
            {
                BOOST_CHECK(err);
                status &= static_cast<bool>(err);
                BOOST_CHECK(msg.empty());
                status &= msg.empty();
            }
        }};
    auto connect_callback { [&sent_message, &status](async_tcp_client* facade, const boost::system::error_code& err) {
            BOOST_CHECK(!err);
            status &= !static_cast<bool>(err);
            auto const r_ { facade->async_send(sent_message) };
            BOOST_CHECK(r_);
            status &= r_;
        }};
    boost::asio::ip::tcp::endpoint server_endpoint( boost::asio::ip::address::from_string("127.0.0.1"), 7990 );
    client.register_connect_callback(std::move(connect_callback));
    client.register_receive_callback(std::move(recv_callback));
    client.register_send_callback(std::move(send_callback));
    client.async_connect(server_endpoint);
    std::thread thread_exec{ [&executor] { executor.run(); } };
    thread_exec.join();
    tcp_server.stop();
    BOOST_TEST(status == true);
}

BOOST_AUTO_TEST_CASE(tcp_client_resolver)
{
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(2s);
    using namespace raspberry_guard::network;
    bool status = true;
    bool failed_to_connect = false;
    async_tcp_client::executor_t executor;
    async_tcp_client client(executor);
    auto fail_resolve_callback { [&status, &failed_to_connect] (async_tcp_client * facade, const auto& err, auto results, uint16_t port)
        {
            BOOST_CHECK(err);
            failed_to_connect = true;
            status &= static_cast<bool>(err);
        }};
    auto success_resolve_callback { [&status] (async_tcp_client * facade, const auto& err, auto results, uint16_t port)
        {
            BOOST_CHECK(!err);
            status &= !static_cast<bool>(err);
            BOOST_CHECK(!results.empty());
            if (!results.empty())
            {
                auto ep { results->endpoint() };
                ep.port(port);
                facade->async_connect(ep);
            }
        }};

    auto connect_callback { [&status](async_tcp_client * facade, const auto& err)
        {
            status &= !static_cast<bool>(err);
            facade->close();
        }};
    client.register_connect_callback(std::move(connect_callback));
    client.register_resolve_callback(std::move(fail_resolve_callback));
    client.async_connect("ZZZZZZZZZ", 80, 3000);
    std::thread thread_exec { [&executor] { executor.run(); } };
    while (!failed_to_connect){ std::this_thread::sleep_for(100ms); }
    client.register_resolve_callback(std::move(success_resolve_callback));
    client.async_connect("google.com", 80, 3000);
    thread_exec.join();
    BOOST_TEST(status == true);
}
