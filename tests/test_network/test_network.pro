TEMPLATE = app
CONFIG -= qt
CONFIG -= app_bundle
CONFIG += console c++17 thread

isEmpty(BOOST_INCLUDE_DIR): BOOST_INCLUDE_DIR=$$(BOOST_INCLUDE_DIR)
!isEmpty(BOOST_INCLUDE_DIR): INCLUDEPATH *= $${BOOST_INCLUDE_DIR}

isEmpty(BOOST_INCLUDE_DIR): {
    message("BOOST_INCLUDE_DIR is not set, assuming Boost can be found automatically in your system")
}

SOURCES += \
    main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../network/release/ -lnetwork
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../network/debug/ -lnetwork
else:unix: LIBS += -L$$OUT_PWD/../../network/ -lnetwork

INCLUDEPATH += $$PWD/../../network
DEPENDPATH += $$PWD/../../network

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../network/release/libnetwork.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../network/debug/libnetwork.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../network/release/network.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../network/debug/network.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../network/libnetwork.a

LIBS += -lboost_serialization
