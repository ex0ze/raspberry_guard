#include <iostream>
#include <random>
#include "network.hpp"

raspberry_guard::network::async_udp_server * _server;

void success_read_callback(boost::asio::ip::udp::endpoint ep, std::vector<char> vec)
{
    std::cout << "Received message from " << ep << " :\n";
    std::copy(std::cbegin(vec), std::cend(vec), std::ostream_iterator<char> {std::cout});
    std::cout << std::endl;
    std::reverse(std::begin(vec), std::end(vec));
    _server -> send_to(ep, std::move(vec));
}

int main()
{
    using namespace std::chrono_literals;
    raspberry_guard::network::async_udp_server server;
    _server = &server;
    server.register_callback_on_read(success_read_callback);
    server.run();
    std::this_thread::sleep_for(2s);
    boost::asio::io_service service;
    boost::asio::ip::udp::socket sock{ service, boost::asio::ip::udp::v4() };
    boost::system::error_code err;
    auto ep {boost::asio::ip::udp::endpoint(boost::asio::ip::address_v4::from_string("127.0.0.1", err), 1337)};
    std::default_random_engine engine;
    std::uniform_int_distribution<int> dist { (int)'a', (int)'z' };
    auto get_random_sym { [&] { return dist(engine); }};

    auto t = std::thread([&]{ service.run(); });
    std::vector<char> buf(20);
    while (true)
    {
        std::generate_n(std::begin(buf), std::size(buf), get_random_sym);
        sock.send_to(boost::asio::buffer(buf.data(), buf.size()), ep);
        sock.receive(boost::asio::buffer(buf.data(), buf.size()));
        std::this_thread::sleep_for(1s);
        std::cout << "Received from server: ";
        std::copy(std::begin(buf), std::end(buf), std::ostream_iterator<char>{std::cout});
        std::cout << std::endl;
    }
    t.join();
    return 0;
}
