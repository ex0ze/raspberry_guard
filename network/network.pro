CONFIG -= qt

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    async_tcp_client.cpp \
    async_tcp_server.cpp \
    async_udp_server.cpp \
    network_message.cpp

HEADERS += \
    async_tcp_client.h \
    async_tcp_server.h \
    async_udp_server.h \
    network.hpp \
    network_message.h \
    network_message_adapter.hpp \
    tcp_connection.hpp

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
