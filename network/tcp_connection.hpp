#ifndef TCP_CONNECTION_H
#define TCP_CONNECTION_H

#include <memory>
#include <vector>
#include <functional>
#include <optional>
#include <boost/asio.hpp>

namespace raspberry_guard
{

namespace network {

class tcp_connection : public std::enable_shared_from_this<tcp_connection>
{
public:
    using write_callback_t =    std::function<void(boost::asio::ip::tcp::endpoint, const boost::system::error_code&)>;
    using read_callback_t =     std::function<void(boost::asio::ip::tcp::endpoint, const boost::system::error_code&, std::vector<char>)>;
    using buffer_t =            std::vector<char>;
    using socket_t =            boost::asio::ip::tcp::socket;

    using pointer = std::shared_ptr<tcp_connection>;
    static inline pointer make_connection(boost::asio::io_service& executor, std::size_t default_buffer_size = 65536)
    {
        return pointer(new tcp_connection(executor, default_buffer_size));
    }
    boost::asio::ip::tcp::socket& socket()
    {
        return (*p_socket);
    }
    /////////////////////////handlers//////////////////////////
    template <typename Func, typename ... Args>
    void register_write_callback(Func && f, Args &&... args)
    {
        m_write_callback = { std::forward<Func>(f), std::forward<Args>(args)... };
    }
    template <typename Func, typename ... Args>
    void register_read_callback(Func && f, Args &&... args)
    {
        m_read_callback = { std::forward<Func>(f), std::forward<Args>(args)... };
    }
    ///////////////////////////////////////////////////////////
    void start_read_async()
    {
        p_socket->async_read_some(boost::asio::buffer(m_read_buffer), [_this = shared_from_this()](const auto& err, auto nbytes)
        {

            if (_this->m_read_callback)
            {
                buffer_t move_buffer;
                if (!err)
                     move_buffer.assign(std::cbegin(_this->m_read_buffer), std::cbegin(_this->m_read_buffer) + nbytes);
                _this->m_read_callback(_this->p_socket->remote_endpoint(), err, std::move(move_buffer));
            }
            if (!_this->m_stop)
                _this->start_read_async();
        });
    }
    template <typename InputIterator>
    void write_async(InputIterator begin, InputIterator end)
    {
        const auto size { std::distance(begin, end) };
        std::copy(begin, end, std::begin(m_read_buffer));
        p_socket->async_write_some(boost::asio::buffer(m_read_buffer.data(), size), [_this = shared_from_this()] (const auto& err, auto bytes)
        {
            if (_this->m_write_callback)
                _this->m_write_callback(_this->p_socket->remote_endpoint(), err);
        });
    }
    void stop()
    {
        if (m_stop)
            return;
        m_stop = true;
        p_socket->close();

    }
private:
    tcp_connection(boost::asio::io_service& executor, std::size_t default_buffer_size = 65536) :
        p_socket( std::make_shared<socket_t>(executor)),
        m_read_buffer(default_buffer_size), //! we use preallocation policy (allocate memory only on creation)
        m_write_buffer(default_buffer_size) {}
    std::shared_ptr<socket_t>       p_socket;
    write_callback_t                m_write_callback;
    read_callback_t                 m_read_callback;
    buffer_t                        m_read_buffer;
    buffer_t                        m_write_buffer;
    bool                            m_stop {false};

};

}

}

#endif // TCP_CONNECTION_H
