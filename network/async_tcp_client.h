#ifndef ASYNC_TCP_CLIENT_H
#define ASYNC_TCP_CLIENT_H

#include <boost/asio.hpp>
#include <memory>
#include <functional>
#include <string_view>

namespace raspberry_guard
{

namespace network
{

class async_tcp_client
{
    class impl_;
    struct impl_deleter { void operator()(impl_* p) const {delete p;} };
    std::unique_ptr<impl_, impl_deleter> p_impl;
public:

    using connect_callback_t =  std::function<void(async_tcp_client*, const boost::system::error_code&)>;
    using receive_callback_t =  std::function<void(async_tcp_client*, const boost::system::error_code&, std::vector<char>)>;
    using send_callback_t =     std::function<void(async_tcp_client*, const boost::system::error_code&, std::size_t)>;
    using resolve_callback_t =  std::function<void(async_tcp_client*, const boost::system::error_code&, boost::asio::ip::tcp::resolver::results_type, uint16_t)>;
    using socket_t =            boost::asio::ip::tcp::socket;
    using executor_t =          boost::asio::io_service;
    using endpoint_t =          boost::asio::ip::tcp::endpoint;
    using resolver_t =          boost::asio::ip::tcp::resolver;
    using buffer_t =            std::vector<char>;
    using timer_t =             boost::asio::steady_timer;

    async_tcp_client(executor_t& executor);
    void async_connect(endpoint_t ep, int64_t deadline_ms = 0LL);
    void async_connect(std::string_view ep, uint16_t port, int64_t deadline_ms = 0LL);
    bool async_send(buffer_t buf);
    void close();
    inline const endpoint_t& get_endpoint() const;
    void register_connect_callback(connect_callback_t);
    void register_receive_callback(receive_callback_t);
    void register_send_callback(send_callback_t);
    void register_resolve_callback(resolve_callback_t);
};

}

}



#endif // ASYNC_TCP_CLIENT_H
