#include "async_tcp_client.h"

namespace raspberry_guard
{

namespace network
{

class async_tcp_client::impl_
{
    async_tcp_client::executor_t& m_executor;
    std::shared_ptr<async_tcp_client::socket_t> p_socket;
    std::shared_ptr<async_tcp_client::endpoint_t> p_endpoint;
    std::shared_ptr<async_tcp_client::resolver_t> p_resolver;
    std::shared_ptr<async_tcp_client::timer_t> p_resolve_timer;
    std::shared_ptr<async_tcp_client::timer_t> p_connect_timer;
    async_tcp_client * p_facade;
    connect_callback_t m_connect_callback;
    receive_callback_t m_receive_callback;
    send_callback_t    m_send_callback;
    resolve_callback_t m_resolve_callback;
    buffer_t           m_buffer;
    bool m_stopped { false };
public:
    impl_(executor_t& executor, async_tcp_client * facade, std::size_t default_buf_size = 65536) :
        m_executor(executor),
        p_socket { std::make_shared<socket_t>( m_executor ) },
        p_resolver { std::make_shared<resolver_t>( m_executor ) },
        p_resolve_timer { std::make_shared<timer_t>( m_executor ) },
        p_connect_timer { std::make_shared<timer_t>( m_executor ) },
        p_facade(facade),
        m_buffer(default_buf_size) {} //! preallocate
    void stop()
    {
        if (m_stopped)
            return;
        m_stopped = true;
        p_socket->close();
        p_resolve_timer->cancel();
        p_connect_timer->cancel();
    }
    void async_connect_(boost::asio::ip::tcp::endpoint ep, int64_t deadline_ms)
    {
        p_endpoint = std::make_shared<endpoint_t>(std::move(ep));
        p_socket->async_connect(ep, [&](const auto& err)
        {
            if (!err)
                p_connect_timer->cancel();
            if (m_connect_callback)
                m_connect_callback(p_facade, err);
            if (!err && !m_stopped)
            {
                async_read_();
            }
        });
        if (deadline_ms > 0)
        {
            p_connect_timer->expires_from_now(std::chrono::milliseconds(deadline_ms));
            p_connect_timer->async_wait( [this](const auto& err)
            {
                if (m_stopped)
                    return;
                p_socket->close();
            });
        }

    }
    void async_resolve_(std::string_view host, uint16_t port, int64_t deadline_ms)
    {
        p_resolver->async_resolve(host, {}, [this, port, deadline_ms](const auto& err, boost::asio::ip::tcp::resolver::results_type results)
        {
            if (!err)
                p_resolve_timer->cancel();
            if (m_resolve_callback)
                m_resolve_callback(p_facade, err, std::move(results), port);
            else
            {
                if (!err)
                {
                    if (!results.empty())
                    {
                        auto ep { results->endpoint() };
                        ep.port(port);
                        async_connect_(ep, deadline_ms);
                    }
                }
            }
        });
        if (deadline_ms > 0)
        {
            p_resolve_timer->expires_from_now(std::chrono::milliseconds(deadline_ms));
            p_resolve_timer->async_wait( [this](const auto& err)
            {
                if (m_stopped)
                    return;
                p_resolver->cancel();
            });
        }
    }
    bool async_send_(buffer_t vec)
    {
        if (!p_socket->is_open()) return false;
        auto shared_vec { std::make_shared<buffer_t>(std::move(vec)) };
        p_socket->async_write_some(boost::asio::buffer(shared_vec->data(), shared_vec->size()), [shared_vec, this](const auto& err, auto nbytes){
            if (m_send_callback)
                m_send_callback(p_facade, err, nbytes);
        });
        return true;
    }
    void register_connect_callback_(connect_callback_t cb)
    {
        m_connect_callback = std::move(cb);
    }
    void register_receive_callback_(receive_callback_t cb)
    {
        m_receive_callback = std::move(cb);
    }
    void register_send_callback_(send_callback_t cb)
    {
        m_send_callback = std::move(cb);
    }
    void register_resolve_callback_(resolve_callback_t cb)
    {
        m_resolve_callback = std::move(cb);
    }
    inline const auto& get_endpoint() const
    {
        return *p_endpoint;
    }
    void close_()
    {
        p_socket->close();
        p_resolve_timer->cancel();
        p_connect_timer->cancel();
    }
private:
    void async_read_()
    {
        p_socket->async_read_some(boost::asio::buffer(m_buffer.data(), m_buffer.size()), [this](const auto& err, auto nbytes)
        {
            if (m_receive_callback)
            {
                buffer_t move_buffer;
                if (!err)
                    move_buffer.assign(std::cbegin(m_buffer), std::cbegin(m_buffer) + nbytes);
                m_receive_callback(p_facade, err, std::move(move_buffer));
            }
            if (!err)
                async_read_();
        });
    }
    void check_deadline_()
    {
        if (m_stopped)
            return;
    }
};

async_tcp_client::async_tcp_client(executor_t &executor) :
    p_impl(new impl_(executor, this))
{

}

inline const async_tcp_client::endpoint_t& async_tcp_client::get_endpoint() const
{
    return p_impl->get_endpoint();
}

void async_tcp_client::async_connect(async_tcp_client::endpoint_t ep, int64_t deadline_ms)
{
    p_impl->async_connect_(std::move(ep), deadline_ms);
}

void async_tcp_client::async_connect(std::string_view ep, uint16_t port, int64_t deadline_ms)
{
    p_impl->async_resolve_(ep, port, deadline_ms);
}

void async_tcp_client::register_connect_callback(async_tcp_client::connect_callback_t cb)
{
    p_impl->register_connect_callback_(std::move(cb));
}

void async_tcp_client::register_receive_callback(async_tcp_client::receive_callback_t cb)
{
    p_impl->register_receive_callback_(std::move(cb));
}

void async_tcp_client::register_send_callback(async_tcp_client::send_callback_t cb)
{
    p_impl->register_send_callback_(std::move(cb));
}

void async_tcp_client::register_resolve_callback(async_tcp_client::resolve_callback_t cb)
{
    p_impl->register_resolve_callback_(std::move(cb));
}

bool async_tcp_client::async_send(async_tcp_client::buffer_t buf)
{
    return p_impl->async_send_(std::move(buf));
}

void async_tcp_client::close()
{
    p_impl->close_();
}

} // namespace network

} // namespace raspberry_guard
