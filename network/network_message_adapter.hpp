#ifndef NETWORK_MESSAGE_ADAPTER_H
#define NETWORK_MESSAGE_ADAPTER_H

#include "network_message.h"
#include <boost/endian/conversion.hpp>
#include <sstream>

namespace raspberry_guard
{

namespace network
{

class network_message_adapter
{
public:
    static std::vector<char> to_vec(raspberry_guard::network::network_message_base const * message)
    {
        using namespace raspberry_guard::network;
        const auto type { message->type() };
        auto typecast { static_cast<network_message_base::underlying_message_type>(type)};
        typecast = boost::endian::native_to_little(typecast);
        std::vector<char> result(sizeof typecast);
        memcpy(result.data(), &typecast, sizeof typecast);
        std::stringstream ss;
        boost::archive::binary_oarchive oarch { ss };
        using nmt = network_message_base::network_message_type;
        switch (type) {
        case nmt::message_base:
            return result;
        case nmt::message_raw:
            oarch << (dynamic_cast<const network_message_raw&>(*message));
            break;
        default:
            return result;
        }
        const auto serialized_contents { ss.str() };
        const auto new_size { result.size() + serialized_contents.size() };
        result.reserve( new_size );
        result.insert(std::end(result), std::cbegin(serialized_contents), std::cend(serialized_contents));
        return result;
    }
    static std::unique_ptr<raspberry_guard::network::network_message_base> from_vec(const std::vector<char>& vec)
    {
        using namespace raspberry_guard::network;
        network_message_base::underlying_message_type undertype{};
        if (vec.size() < sizeof undertype) return {};
        memcpy(&undertype, vec.data(), sizeof undertype);
        undertype = boost::endian::little_to_native(undertype);
        network_message_base::network_message_type type { undertype };
        if (!network_message_base::is_valid_type(type)) return {};
        std::stringstream ss;
        ss << std::string{vec.cbegin() + sizeof undertype, vec.cend()};
        boost::archive::binary_iarchive iarch { ss };
        using nmt = network_message_base::network_message_type;
        switch (type) {
        case nmt::message_base:
            return std::make_unique<network_message_base>(type);
        case nmt::message_raw:
            auto ret { std::make_unique<network_message_raw>() };
            iarch >> (*ret);
            return ret;
        }
    }
};

} // namespace network

} // namespace raspberry_guard

#endif // NETWORK_MESSAGE_ADAPTER_H
