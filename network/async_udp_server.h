#ifndef ASYNC_UDP_SERVER_H
#define ASYNC_UDP_SERVER_H

#include <boost/asio.hpp>
#include <memory>
#include <iostream>
#include <iterator>
#include <optional>
#include <vector>
#include <functional>
#include <string>

namespace raspberry_guard
{

namespace network
{

class async_udp_server
{
    class impl_;
    struct impl_deleter { void operator()(impl_ * p) const {delete p;} };
    std::unique_ptr<impl_, impl_deleter> p_impl;
public:
    async_udp_server();
    ~async_udp_server() = default;
    void register_callback_on_read(std::function<void(boost::asio::ip::udp::endpoint, std::vector<char>)> cb);
    void send_to(const boost::asio::ip::udp::endpoint& ep, std::vector<char> buf);
    void run();
    void stop();
};


} // namespace network

} // namespace raspberry_guard


#endif // ASYNC_UDP_SERVER_H
