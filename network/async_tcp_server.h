#ifndef ASYNC_TCP_SERVER_H
#define ASYNC_TCP_SERVER_H

#include <memory>
#include <boost/asio.hpp>
#include <thread>
#include <map>
#include <vector>

#include "tcp_connection.hpp"

namespace raspberry_guard
{

namespace network
{

class async_tcp_server
{
private:
    class impl_;
    struct impl_deleter { void operator()(impl_* p) const {delete p;} };
    std::unique_ptr<impl_, impl_deleter> p_impl;
public:

    using read_callback_t =     std::function<void(async_tcp_server*, boost::asio::ip::tcp::endpoint, const boost::system::error_code &, std::vector<char>)>;
    using write_callback_t =    std::function<void(async_tcp_server*, boost::asio::ip::tcp::endpoint, const boost::system::error_code&)>;
    using buffer_t =            std::vector<char>;
    using endpoint_t =          boost::asio::ip::tcp::endpoint;
    async_tcp_server(uint16_t port);
    void run(int num_threads);
    bool is_running() const;
    void register_read_callback(read_callback_t);
    void register_write_callback(write_callback_t);
    bool async_send_to(const endpoint_t& ep, const buffer_t& buf);
    void stop();
    const impl_ * impl() const { return p_impl.get(); }
    impl_ * impl() { return p_impl.get(); }
};

} // namespace network

} // namespace raspberry_guard
#endif // ASYNC_TCP_SERVER_H
