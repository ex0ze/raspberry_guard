#include "async_udp_server.h"
#include <thread>
namespace raspberry_guard
{

namespace network
{

class async_udp_server::impl_
{
public:
    impl_() :
        m_ioservice(),
        m_socket(m_ioservice, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), 1337)),
        m_buffer(16384) {}
    void run()
    {
        m_stop = false;
        start_receive();
        m_ios_worker = std::thread( [&]{ m_ioservice.run(); } );
    }
    void stop()
    {
        if (m_stop)
            return;
        m_stop = true;
        m_ioservice.stop();
        if (m_ios_worker.joinable())
            m_ios_worker.join();
    }
    void send_to(const boost::asio::ip::udp::endpoint& ep, std::vector<char> buf)
    {
        auto sbuf { std::make_shared<std::vector<char>>(std::move(buf)) };
        m_socket.async_send_to(boost::asio::buffer(sbuf->data(), sbuf->size()), ep,
                               [keepalive = sbuf](const auto& err, auto bytes) {});
    }
    ~impl_()
    {
        stop();
    }
    void reg_cb_read(std::function<void (const boost::asio::ip::udp::endpoint &, std::vector<char>)> cb)
    {
        m_callback_on_read = std::move(cb);
    }
private:
    void start_receive()
    {
        if (m_stop)
            return;
        m_socket.async_receive_from(boost::asio::buffer(m_buffer), m_remote_endpoint, [&](const auto &err, auto bytes)
        {
            if (!err || err == boost::asio::error::message_size)
            {
                if (m_callback_on_read)
                {
                    std::vector<char> move_buffer(m_buffer.begin(), m_buffer.begin() + bytes);
                    m_callback_on_read(m_remote_endpoint, std::move(move_buffer));
                }
            }
            start_receive();
        });
    }
    std::thread m_ios_worker;
    boost::asio::io_service m_ioservice;
    boost::asio::ip::udp::socket m_socket;
    boost::asio::ip::udp::endpoint m_remote_endpoint;
    std::vector<char> m_buffer;
    bool m_stop {false};
    std::function<void (const boost::asio::ip::udp::endpoint &, std::vector<char>)> m_callback_on_read;
};

async_udp_server::async_udp_server() : p_impl(new impl_)
{
}

void async_udp_server::send_to(const boost::asio::ip::udp::endpoint &ep, std::vector<char> buf)
{
    p_impl->send_to(ep, std::move(buf));
}

void async_udp_server::register_callback_on_read(std::function<void (boost::asio::ip::udp::endpoint, std::vector<char>)> cb)
{
    p_impl->reg_cb_read(std::move(cb));
}

void async_udp_server::run()
{
    p_impl->run();
}

void async_udp_server::stop()
{
    p_impl->stop();
}



} // namespace network

} // namespace raspberry_guard
