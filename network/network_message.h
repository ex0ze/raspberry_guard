#ifndef NETWORK_MESSAGE_H
#define NETWORK_MESSAGE_H

#include <vector>
#include <cstdint>
#include <boost/serialization/access.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include "boost/serialization/vector.hpp"
namespace raspberry_guard
{

namespace network
{

class network_message_base
{
public:
    enum class network_message_type : int32_t
    {
        possible_interval_begin = 0,
        message_base    = 0,
        message_raw     = 1,
        possible_interval_end = 1
    };
    using underlying_message_type = std::underlying_type_t<network_message_type>;
    explicit network_message_base(network_message_type type = network_message_type::message_base) : m_type(type) {}
    auto inline type() const
    {
        return m_type;
    }
    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version) { (void)ar; (void)version; }
    inline static bool is_valid_type(network_message_type t)
    {
        return
                (static_cast<underlying_message_type>(network_message_type::possible_interval_begin) <= static_cast<underlying_message_type>(t))
                &&
                static_cast<underlying_message_type>(t) <= static_cast<underlying_message_type>(network_message_type::possible_interval_end);
    }
    virtual ~network_message_base() = default;
private:
    network_message_type m_type;
};

class network_message_raw : public network_message_base
{
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        (void)version;
        ar & m_raw_message;
    }
    std::vector<char> m_raw_message;

public:
    network_message_raw(std::vector<char> raw = std::vector<char>()) :
        network_message_base(network_message_base::network_message_type::message_raw),
        m_raw_message(std::move(raw)) {}
    inline bool operator==(const network_message_raw& rhs) const
    {
        return this->m_raw_message == rhs.m_raw_message;
    }

};

} // namespace network

} // namespace raspberry_guard

#endif // NETWORK_MESSAGE_H
