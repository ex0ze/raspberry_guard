#include "async_tcp_server.h"
namespace raspberry_guard
{

namespace network
{

class async_tcp_server::impl_
{
public:
    using executor_t = boost::asio::io_service;
    using acceptor_t = boost::asio::ip::tcp::acceptor;
    using client_map_t = std::map<endpoint_t, std::weak_ptr<tcp_connection>>;
private:
    executor_t m_ioservice;
    acceptor_t m_acceptor;
    std::vector<std::thread> m_threads;
    client_map_t m_clients;
    async_tcp_server::read_callback_t m_read_callback;
    async_tcp_server::write_callback_t m_write_callback;
    async_tcp_server * p_facade;
public:
    impl_(uint16_t port, async_tcp_server * facade) : m_acceptor(m_ioservice, endpoint_t(boost::asio::ip::tcp::v4(), port)), p_facade(facade) {}
    void run(int num_threads)
    {
        start_accept();
        for (int i {0}; i < num_threads; ++i)
            m_threads.emplace_back([&] { m_ioservice.run(); });

    }
    void stop()
    {
        m_ioservice.stop();
        for (auto& t : m_threads)
            if (t.joinable()) t.join();
        m_threads.clear();
    }
    bool async_send_to(const endpoint_t& ep, const buffer_t &buf)
    {
        auto find_iter { m_clients.find(ep) }; //!try to find endpoint
        if (find_iter != std::end(m_clients)) //! if we found endpoint
        {
            auto maybe_ptr { find_iter->second.lock() }; //! try lo get shared_ptr by locking weak_ptr
            if (maybe_ptr) //! if shared_ptr is still valid
            {
                maybe_ptr->write_async(std::cbegin(buf), std::cend(buf));
                return true;
            }
            else //! if shared_ptr has expired
            {
                m_clients.erase(find_iter);
                return false;
            }
        }
        return false;
    }
    void register_read_callback(async_tcp_server::read_callback_t cb)
    {
        m_read_callback = std::move(cb);
    }
    void register_write_callback(async_tcp_server::write_callback_t cb)
    {
        m_write_callback = std::move(cb);
    }
    bool is_running() const
    {
        return !m_ioservice.stopped();
    }
    ~impl_()
    {
        stop();
    }
private:
    void start_accept()
    {
        auto connection { tcp_connection::make_connection(m_ioservice) };
        m_acceptor.async_accept(connection->socket(), std::bind(&impl_::handle_accept, this, connection, std::placeholders::_1));

    }
    void handle_accept(tcp_connection::pointer connection, const boost::system::error_code& error)
    {
        if (!error)
        {
            m_clients.emplace(connection->socket().remote_endpoint(), connection->weak_from_this());
            connection->register_read_callback([this](auto ep, const auto& err, auto buf) //register forwarder callback to automatically erase clients map on errors
            {
                m_read_callback(p_facade, ep, err, std::move(buf)); // internal lambda arity reduction
                if (err)
                    error_handler(ep);
            });
            connection->register_write_callback( std::bind(m_write_callback, p_facade, std::placeholders::_1, std::placeholders::_2) ); // std::bind arity reduction
            connection->start_read_async();
        }
        start_accept();
    }
    inline void error_handler(const boost::asio::ip::tcp::endpoint& ep)
    {
        m_clients.erase(ep);
    }
};

async_tcp_server::async_tcp_server(uint16_t port) : p_impl (new impl_(port, this)) {}

void async_tcp_server::run(int num_threads)
{
    p_impl->run(num_threads);
}

bool async_tcp_server::is_running() const
{
    return p_impl->is_running();
}

void async_tcp_server::register_read_callback(async_tcp_server::read_callback_t cb)
{
    p_impl->register_read_callback(std::move(cb));
}

void async_tcp_server::register_write_callback(async_tcp_server::write_callback_t cb)
{
    p_impl->register_write_callback(std::move(cb));
}

bool async_tcp_server::async_send_to(const endpoint_t& ep, const buffer_t &buf)
{
    return p_impl->async_send_to(ep, buf);
}

void async_tcp_server::stop()
{
    p_impl->stop();
}

} //namespace network

} //namespace raspberry_guard
